import { Container, Row, Col } from "react-bootstrap";
import { BlogItem } from "./BlogItem";

const Layout = () => (
  <>
    <Container className="container-fluid">
      <Row>
        <Col className="pt-5">
          <h1 className="my-auto font-weight-bolder">
            <strong>WELCOME TO PAJAKKU BLOG</strong>
          </h1>
        </Col>
      </Row>
      <BlogItem />
    </Container>
  </>
);

export default Layout;
