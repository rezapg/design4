import { Col, Row } from "react-bootstrap";
import { stockData } from "./data";
import Link from "next/link";

export const BlogItem = () => {
  return (
    <>
      <Row className="pt-5">
        <Col className="my-auto font-weight-bolder">
          {stockData.map((data, key) => {
            return (
              <Link href={`/detail/${data.id}`}>
                <a key={key}>
                  <BlogItems
                    key={key}
                    judul={data.judul}
                    label={data.label}
                    timeElapsed={data.timeElapsed}
                  />
                </a>
              </Link>
            );
          })}
        </Col>
      </Row>
    </>
  );
};

const BlogItems = ({ judul, label, isi, timeElapsed }) => {
  if (!judul) return <div />;
  return (
    <table>
      <tbody>
        <tr>
          <td>
            <h5>{judul}</h5>
          </td>
          <td>
            <h5>{label}</h5>
          </td>
          <td>
            <h4>{isi}</h4>
          </td>
          <td>
            <p>{timeElapsed}</p>
          </td>
        </tr>
      </tbody>
    </table>
  );
};
