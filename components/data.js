export const stockData = [
  {
    id: 1,
    judul: "Fairly New React",
    label: "TWTR",
    isi:
      "The link component is fairly new for react-md and I'm still trying to figure out some reasonable defaults for it. You will most likely want to change the following scss variables for your app",
    timeElapsed: "5 sec ago",
  },
  {
    id: 2,
    judul: "React MD",
    label: "SQ",
    isi:
      "The link component is fairly new for react-md and I'm still trying to figure out some reasonable defaults for it. You will most likely want to change the following scss variables for your app",
    timeElapsed: "10 sec ago",
  },
  {
    id: 3,
    judul: "Pajakku Inc",
    label: "SHOP",
    isi:
      "The link component is fairly new for react-md and I'm still trying to figure out some reasonable defaults for it. You will most likely want to change the following scss variables for your app",
    timeElapsed: "3 sec ago",
  },
  {
    id: 4,
    judul: "Holyday Ayey",
    label: "RUN",
    isi:
      "The link component is fairly new for react-md and I'm still trying to figure out some reasonable defaults for it. You will most likely want to change the following scss variables for your app",
    timeElapsed: "4 sec ago",
  },
  {
    id: 5,
    judul: "Adobe XD is The Best",
    label: "ADBE",
    isi:
      "The link component is fairly new for react-md and I'm still trying to figure out some reasonable defaults for it. You will most likely want to change the following scss variables for your app",
    timeElapsed: "10 sec ago",
  },
  {
    id: 6,
    judul: "Component React is is is ",
    label: "HUBS",
    isi:
      "The link component is fairly new for react-md and I'm still trying to figure out some reasonable defaults for it. You will most likely want to change the following scss variables for your app",
    timeElapsed: "12 sec ago",
  },
];
