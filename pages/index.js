import Head from "next/head";
import NavBar from "../components/NavBar";
import Layout from "../components/Layout";

const Home = () => (
  <>
    <Head>
      <title>Create Next App</title>
      <link rel="icon" href="/favicon.ico" />
    </Head>

    <NavBar />
    <Layout />
  </>
);

export default Home;
